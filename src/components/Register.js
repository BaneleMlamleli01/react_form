import React, { Component } from 'react'
import {Button, Form} from 'react-bootstrap';
import {Link} from "react-router-dom";

const initialState = {
    firstname: '',
    lastname:'',
    email:'',
    sex:'',
    password:'',
    confirmPassword:'',
    employed:'',
    firstnameError: '',
    lastnameError:'',
    emailError:'',
    sexError:'',
    passwordError:'',
    confirmPasswordError:'',
    employedError:''
}

export class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            firstname: '',
            lastname:'',
            email:'',
            sex:'',
            password:'',
            confirmPassword:'',
            employed:'',
            firstnameError: '',
            lastnameError:'',
            emailError:'',
            sexError:'',
            passwordError:'',
            confirmPasswordError:'',
            employedError:''
        }
    }

    // Handler function that will handle the onChange event
    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    // handleFirstnameChange = event => {
    //     this.setState({
    //         firstname: event.target.value
    //     })
    // }

    validateFormField = () => {
        let firstnameError = '';
        let lastnameError = '';
        let emailError = '';
        let sexError = '';
        let passwordError = '';
        let employedError = '';
        let confirmPasswordError = '';

        if(!this.state.firstname) firstnameError = "Firstname field cannot be empty";
        if(!this.state.lastname) lastnameError = "Lastname field cannot be empty";
        if(!this.state.password) passwordError = "Something wrong with the password field";
        if(!this.state.confirmPassword) confirmPasswordError = "Password mismatch or empty";
        if(!this.state.email) emailError = "Email field cannot be empty";
        if(!this.state.email.includes("@")) emailError = "Invalid E-Mail address";

        if(this.state.sex.includes("selectValue")) sexError = "Select sex";
        if(this.state.employed.includes("selectValue")) employedError = "Select employment status";

        if(emailError || firstnameError || lastnameError || passwordError || sexError || employedError || confirmPasswordError){
            this.setState({emailError, firstnameError, lastnameError, passwordError, sexError, employedError, confirmPasswordError});
            return false;
        }
        return true;
    }

    handleSubmit = event => {
        const data = this.state
        const isValid = this.validateFormField();

        if(isValid){
            alert("Hello, " + data.firstname + "\nYou have successfully registered.\n\nGo back to login screen");
            localStorage.setItem("email", data.email);
            localStorage.setItem("password", data.password);
            console.log(data);
            this.setState(initialState);
        }
        event.preventDefault()
    }

    render() {
        const {firstname, lastname, email, sex, password, confirmPassword, employed} = this.state;
        return (
            <React.Fragment >
                <h1 style={{paddingTop: 100}}>Registration form</h1>
                <Form onSubmit={this.handleSubmit}>
                    <div className="form-group custom-center-div">
                        <Form.Group>
                            <Form.Control type="text" value={firstname} placeholder="Firstname" name="firstname" onChange={this.handleChange} />
                            <div style={{fontSize: 12, color: "red"}}>
                                {this.state.firstnameError}
                            </div>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control type="text" value={lastname} placeholder="Lastname" name="lastname" onChange={this.handleChange} />
                            <div style={{fonSize: 12, color: "red"}}>
                                {this.state.lastnameError}
                            </div>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control type="email" value={email} placeholder="email" name="email" onChange={this.handleChange} />
                            <div style={{fontSize: 12, color: "red"}}>
                                {this.state.emailError}
                            </div>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control as="select" type="text" value={sex} name="sex" onChange={this.handleChange}>
                                <option value="selectValue">Select sex</option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                                <option value="other">Other</option>
                            </Form.Control>
                            <div style={{fontSize: 12, color: "red"}}>
                                {this.state.sexError}
                            </div>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control as="select" type="text" value={employed} name="employed" onChange={this.handleChange}>
                                <option value="selectValue">Select employment status</option>
                                <option value="unemployeed">Unemployeed</option>
                                <option value="employed">Employed</option>
                                <option value="freelancer">Freelancer</option>
                            </Form.Control>
                            <div style={{fontSize: 12, color: "red"}}>
                                {this.state.employedError}
                            </div>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control type="password" value={password} placeholder="password" name="password" onChange={this.handleChange} />
                            <div style={{fontSize: 12, color: "red"}}>
                                {this.state.passwordError}
                            </div>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control type="password" value={confirmPassword} placeholder="confirmPassword" name="confirmPassword" onChange={this.handleChange} />
                            <div style={{fontSize: 12, color: "red"}}>
                                {this.state.confirmPasswordError}
                            </div>
                        </Form.Group>
                        <Button type="submit" className="btn btn-primary btn-lg btn-block">Register</Button>
                        <Link to="/Login">Go back to LOGIN screen</Link>
                    </div>
                </Form>
            </React.Fragment>
        )
    }
}

export default Register
