import React, { Component } from 'react';
import {ButtonToolbar, Button, Jumbotron, Table} from 'react-bootstrap';
import JsonData from '../asset/table';
import {Link} from "react-router-dom";
import {ModalComponent} from "./ModalComponent";

export class Dashboard extends Component {
    constructor(props){
        super(props);
        this.state={showModal : false}
    }

    editItem(data, index){
        console.log(index + ": " + data.fruit +" : R" + data.cost);
        sessionStorage.setItem("itemID", index);
        sessionStorage.setItem("fruit", data.fruit);
        sessionStorage.setItem("cost", data.cost);
        this.props.history.push("/Profile");
    };

    modalClose = () =>{
        this.setState({showModal: false});
    }

    // saveProfile = () =>{
    //     alert('Save profile');
    //     this.setState({showModal: false});
    // };

    render() {
        return(
            <>
                <Jumbotron fluid style={{background: '#bafaff'}}>
                    <h1>Dashboard</h1>
                    <p>Welcome to Valhalla. Praise to Valkyrie, you have reached your destination. Feast with us and rejoice!!</p>
                    <Link to="/login">LOGOUT</Link>
                </Jumbotron>
                <ButtonToolbar>
                    <Button variant="primary" size="lg" className="Modal-Button" onClick={()=> this.setState({showModal: true})}>Add profile</Button>
                    <ModalComponent
                        show={this.state.showModal}
                        onHide={this.modalClose}
                        // saveChanges={this.saveProfile}
                    />
                </ButtonToolbar>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Fruit</th>
                            <th>Cost</th>
                        </tr>
                    </thead>
                    <tbody>
                    {JsonData.map((data, index) =>{
                        return <tr>
                             <td><a onClick={this.editItem.bind(this, data, index)}>{index}</a></td>
                            <td><a onClick={this.editItem.bind(this, data, index)}>{data.fruit}</a></td>
                           <td><a onClick={this.editItem.bind(this, data, index)}>R{data.cost}</a></td>
                        </tr>

                    })}
                    </tbody>
                </Table>
            </>
        );
    }
}

export default Dashboard