import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {Button, Form} from 'react-bootstrap';
import JsonData from '../asset/table.json';

const initialState = {
    itemName:'',
    cost:'',
    itemNameError:'',
    costError:''
}

export class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            itemName:sessionStorage.getItem("fruit"),
            cost:sessionStorage.getItem("cost"),
            itemNameError:'',
            costError:''
        }
    }

// Handler function that will handle the onChange event
    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    validateFormField = () => {
        let itemNameError = '';
        let costError = '';
        if(!this.state.itemName) itemNameError = "Item name field cannot be empty";
        if(!this.state.cost) costError = "Cost field cannot be empty";
        if(this.state.cost < 0) costError = "Cost field cannot be a negative value";

        if(itemNameError || costError){
            this.setState({itemNameError, costError});
            return false;
        }
        return true;
    };

    handleSubmit = event => {
        const data = this.state;
        const isValid = this.validateFormField();
        if(isValid){
            console.log(data);
            JsonData.map((key, index) =>{
                if (sessionStorage.getItem("itemID").match(index)){
                    key.fruit = data.itemName;
                    key.cost = data.cost;
                    console.log(key);
                }
            });
            console.log(JsonData);
            this.setState(initialState);
        }
        event.preventDefault()
    }

    render() {
        const {itemName, cost}  = this.state;
        return(
            <React.Fragment>
                <h1 style={{paddingTop: 100}}>Edit Selected Item</h1>
                <Form onSubmit={this.handleSubmit}>
                    <div className="form-group custom-center-div">
                        <Form.Label>
                            Item id: { sessionStorage.getItem("itemID")}
                        </Form.Label>
                        <Form.Group>
                            <Form.Control type="text" value={itemName} placeholder="item name" name="itemName" onChange={this.handleChange} />
                            <div style={{fonSize: 12, color: "red"}}>
                                {this.state.itemNameError}
                            </div>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control type="number" value={cost} placeholder="cost" name="cost" onChange={this.handleChange} />
                            <div style={{fontSize: 12, color: "red"}}>
                                {this.state.costError}
                            </div>
                        </Form.Group>
                        <Button type="submit" className="btn btn-primary btn-lg btn-block">Update Item</Button>
                        <Link to="/Dashboard">Go back to the Items list</Link>
                    </div>
                </Form>
            </React.Fragment>
        );
    }
}

export default Profile