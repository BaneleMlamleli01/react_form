import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {Button, Form} from 'react-bootstrap';

const initialState = {
    email:'',
    password:'',
    emailError:'',
    passwordError:''
}

let incorrectLoginCounter = 0;

export class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email:'',
            password:'',
            emailError:'',
            passwordError:''
        }
    }

// Handler function that will handle the onChange event
    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    validateFormField = () => {
        let emailError = '';
        let passwordError = '';
        if(!this.state.password) passwordError = "Something wrong with the password field";
        if(!this.state.email) emailError = "Email field cannot be empty";
        if(!this.state.email.includes("@")) emailError = "Invalid E-Mail address";

        if(emailError || passwordError){
            this.setState({emailError, passwordError});
            return false;
        }

        return true;
    }

    handleSubmit = event => {
        const data = this.state;
        const isValid = this.validateFormField();
        if(isValid){
            console.log(data);
            if ((data.email === localStorage.getItem("email")) && (data.password === localStorage.getItem("password"))){
                incorrectLoginCounter = 0;
                this.props.history.push("/Dashboard");
            }else{
                incorrectLoginCounter += 1;
                alert("Incorrect login details");
            }
            if (incorrectLoginCounter >= 3) alert("3 INCORRECT LOGIN ATTEMPTS\nYOUR ACCOUNT IS LOCKED!!");
            this.setState(initialState);
        }
        event.preventDefault()
    }

    render() {
        const {email, password} = this.state;
        return(
            <React.Fragment>
                <h1 style={{paddingTop: 100}}>Login screen</h1>
                <Form onSubmit={this.handleSubmit}>
                    <div className="form-group custom-center-div">
                        <Form.Group>
                            <Form.Control type="email" value={email} placeholder="email" name="email" onChange={this.handleChange} />
                            <div style={{fontSize: 12, color: "red"}}>
                                {this.state.emailError}
                            </div>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control type="password" value={password} placeholder="password" name="password" onChange={this.handleChange} />
                            <div style={{fontSize: 12, color: "red"}}>
                                {this.state.passwordError}
                            </div>
                        </Form.Group>
                        <Button type="submit" className="btn btn-primary btn-lg btn-block">Login</Button>
                        <Link to="/Register">Register as a new user</Link>
                    </div>
                </Form>
            </React.Fragment>
        );
    }
}

export default Login