import React, { Component } from 'react';
import {Modal, Button, Form} from 'react-bootstrap';
import JsonData from "../asset/table";


export class ModalComponent extends Component {
    constructor(props) {
        super(props);
    }

    handleSubmit = event => {
        alert(event.target.itemName.value);
    }

    render() {
        return (
            <Modal {...this.props} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Add profile
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <React.Fragment>
                        <Form onSubmit={this.handleSubmit}>
                            <div className="form-group custom-center-div">
                                <Form.Group>
                                    <Form.Control type="text" placeholder="item name" name="itemName" required/>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Control type="number" placeholder="cost" name="cost" required/>
                                </Form.Group>
                                <Button type="submit" className="btn btn-primary btn-lg btn-block">Save profile</Button>
                            </div>
                        </Form>
                    </React.Fragment>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={this.props.onHide}>Close</Button>
                    {/*<Button variant="primary" type="submit" onClick={this.props.saveChanges}>Save Changes</Button>*/}
                </Modal.Footer>
            </Modal>
        );
    }
}

export default ModalComponent;