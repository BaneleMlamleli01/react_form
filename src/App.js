import React from 'react';
import './App.css';
import Register from './components/Register'
import Login from './components/Login'
import Profile from './components/Profile'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Dashboard from "./components/Dashboard";

function App() {
  return (
      <Router>
          <div className="App">
              <Switch>
                  <Route path="/" exact component={Login}/>
                  <Route path="/login" exact component={Login}/>
                  <Route path="/register" component={Register}/>
                  <Route path="/dashboard" component={Dashboard}/>
                  <Route path="/profile" component={Profile}/>
              </Switch>
          </div>
      </Router>
  );
}

export default App;
